﻿using Moq;
using NUnit.Framework;
using OrderManager.Client.Models;
using OrderManager.Client.Services;
using OrderManager.Client.ViewModels;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.UnitTests
{
    //TODO: Other tests and proper configuration
    [TestFixture]
    public class PlaceOrderViewModelTests 
    {
        private List<OrderItemModel> _orderItems;
        private List<ProductData> _products;
        private List<CustomerData> _customers;
        private Mock<IPlaceOrderService> _serviceMock;
        private Mock<IDialogService> _dialogMock;

        [SetUp]
        public void Initialize()
        {
            _orderItems = new List<OrderItemModel>();
            _customers = new List<CustomerData>();
            _products = new List<ProductData>();

            _serviceMock = new Mock<IPlaceOrderService>();
            _dialogMock = new Mock<IDialogService>();

            _serviceMock.Setup(m => m.GetProducts()).Returns(Task.FromResult(_products));
            _serviceMock.Setup(m => m.GetCustomers()).Returns(Task.FromResult(_customers));
            _serviceMock.Setup(m => m.CreateOrder(It.IsAny<OrderData>())).Callback<OrderData>(order => {/* TODO:*/ });

            _dialogMock.Setup(d => d.ShowNotification(It.IsAny<string>(), It.IsAny<string>()));

           // _serviceMock.Setup(m => m.CreateOrder())
            _products.Add(new ProductData
            {
                Description = "Product 1 Desc",
                Name = "Product 1",
                Id = 1
            });
            _products.Add(new ProductData
            {
                Description = "Product 2 Desc",
                Name = "Product 2",
                Id = 2
            });
            _products.Add(new ProductData
            {
                Description = "Product 3 Desc",
                Name = "Product 3",
                Id = 3
            });
        }

        [Test]
        public void PlaceOrder_CanExecuteWithNeededData()
        {
            var viewModel = new PlaceOrderViewModel(_serviceMock.Object, _dialogMock.Object);
            viewModel.SelectedCustomer = new CustomerData();
            viewModel.AddOrderItem(_products.FirstOrDefault());

            Assert.IsTrue(viewModel.CanCreateOrder);
        }

        [Test]
        public void PlaceOrder_DisabledWithEmptyCustomer()
        {
            var viewModel = new PlaceOrderViewModel(_serviceMock.Object, _dialogMock.Object);
            viewModel.SelectedCustomer = null;
            viewModel.AddOrderItem(_products.FirstOrDefault());

            Assert.IsFalse(viewModel.CanCreateOrder);
        }

        [Test]
        public void PlaceOrder_DisabledWithEmptyOrderItems()
        {
            var viewModel = new PlaceOrderViewModel(_serviceMock.Object, _dialogMock.Object);
            viewModel.SelectedCustomer = new CustomerData();

            Assert.IsFalse(viewModel.CanCreateOrder);
        }

        [Test]
        public void LoadProducts_AreProductsLoaded()
        {
            var viewModel = new PlaceOrderViewModel(_serviceMock.Object, _dialogMock.Object);
            viewModel.LoadProducts();

            CollectionAssert.AreEqual(_products, viewModel.ProductsList);
        }

        [Test]
        public void LoadProducts_IsAppNotBusyAfterLoad()
        {
            var viewModel = new PlaceOrderViewModel(_serviceMock.Object, _dialogMock.Object);
            viewModel.LoadProducts();

            Assert.IsTrue(viewModel.IsAppNotBusy);
        }
    }
}
