﻿using System.Runtime.Serialization;

namespace OrderManager.Entities
{
    [DataContract]
    public partial class OrderItemData
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long OrderId { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public decimal UnitPrice { get; set; }

        [DataMember]
        public decimal TotalPrice { get; set; }

        [DataMember]
        public ProductData Product { get; set; }

        [DataMember]
        public int OrderItemStatusId { get; set; }

        public OrderItemStatusType OrderItemStatus
        {
            get
            {
                return (OrderItemStatusType)OrderItemStatusId;
            }
            set
            {
                OrderItemStatusId = (int)value;
            }
        }
    }
}
