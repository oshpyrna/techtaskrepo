﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace OrderManager.Entities
{
    [DataContract]
    public class OrderData
    {
        public OrderData()
        {
            OrderItems = new List<OrderItemData>();
        }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public Guid CustomerId { get; set; }

        [DataMember]
        public int OrderStatusId { get; set; }

        public OrderStatusType OrderStatus
        {
            get
            {
                return (OrderStatusType)OrderStatusId;
            }
            set
            {
                OrderStatusId = (int)value;
            }
        }

        [DataMember]
        public CustomerData Customer { get; set; }

        [DataMember]
        public DateTime OrderDate { get; set; }

        [DataMember]
        public decimal ItemsTotal { get; set; }

        [DataMember]
        public List<OrderItemData> OrderItems { get; set; }
    }
}
