﻿
namespace OrderManager.Entities
{
    public enum OrderStatusType
    { 
        Completed = 1,
        PartiallyCompleted = 2,
        Pending = 5
    }
}
