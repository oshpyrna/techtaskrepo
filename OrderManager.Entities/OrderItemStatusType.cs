﻿
namespace OrderManager.Entities
{
    public enum OrderItemStatusType
    {
        Created = 1,
        Processed = 2,
        NotInStock = 3
    }
}
