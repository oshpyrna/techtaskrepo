﻿using System.Runtime.Serialization;

namespace OrderManager.Entities
{
    [DataContract]
    public class ProductData
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
