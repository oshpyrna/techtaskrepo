﻿
namespace OrderManager.Entities
{
    public class OrderStatusData
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
