﻿using System;
using System.Runtime.Serialization;

namespace OrderManager.Entities
{
    [DataContract]
    public class CustomerData
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Zip { get; set; }

        //Need to have set block or will get obscure error during serialization
        [DataMember]
        public string FullName { get { return $"{FirstName} {LastName}"; } set { } }
    }
}
