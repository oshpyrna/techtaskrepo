﻿using Caliburn.Micro;
using OrderManager.Client.Models;
using OrderManager.Client.Services;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderManager.Client.ViewModels
{
    public class PlaceOrderViewModel : BaseViewModel
    {
        private readonly IPlaceOrderService _orderService;
        private readonly IDialogService _dialogService;

        private CustomerData _selectedCustomer;
        public CustomerData SelectedCustomer
        {
            get
            {
                return _selectedCustomer;
            }
            set
            {
                _selectedCustomer = value;
                NotifyOfPropertyChange(() => SelectedCustomer);
                NotifyOfPropertyChange(() => CanCreateOrder);
            }
        }

        public BindableCollection<CustomerData> CustomersList { get; set; }

        public BindableCollection<ProductData> ProductsList { get; set; }

        public BindableCollection<OrderItemModel> OrderItemsList { get; set; }

        public PlaceOrderViewModel(IPlaceOrderService orderService, IDialogService dialogService)
        {
            _orderService = orderService;
            _dialogService = dialogService;

            CustomersList = new BindableCollection<CustomerData>();
            ProductsList = new BindableCollection<ProductData>();
            OrderItemsList = new BindableCollection<OrderItemModel>();

            OrderItemsList.CollectionChanged += (s, e) => { NotifyOfPropertyChange(() => CanCreateOrder); };
        }

        public async void LoadProducts()
        {
            ProductsList.Clear();

            IsAppNotBusy = false;

            try
            {
                var products = await _orderService.GetProducts();
                ProductsList.AddRange(products);
            }
            catch (Exception ex)
            {
                //TODO: logging and notification
            }

            IsAppNotBusy = true;
        }

        public async void LoadCustomers()
        {
            CustomersList.Clear();

            IsAppNotBusy = false;

            try
            {
                var customers = await _orderService.GetCustomers();
                CustomersList.AddRange(customers);
            }
            catch (Exception ex)
            {
                //TODO: logging and notification
            }

            IsAppNotBusy = true;
        }

        public bool CanCreateOrder
        {
            get
            {
                return SelectedCustomer != null && OrderItemsList.Count > 0;
            }
        }

        public async void CreateOrder()
        {
            IsAppNotBusy = false;

            try
            {
                var newOrder = new OrderData
                {
                    CustomerId = SelectedCustomer.Id,
                    OrderStatus = OrderStatusType.Pending,
                    OrderItems = new List<OrderItemData>(OrderItemsList.Select(oil => oil.GetOrderItem())),
                };

                var newId = await _orderService.CreateOrder(newOrder);
                _dialogService.ShowNotification("Order Created Successfully", "Order Id: " + newId);
            }
            catch (Exception ex)
            {
                //TODO: logging and correct notification
            }

            IsAppNotBusy = true;
        }

        public void AddOrderItem(ProductData product)
        {
            if (!OrderItemsList.Any(oil => oil.Product == product))
            {
                OrderItemsList.Add(new OrderItemModel(product));
            }
        }

        public void ClearOrderItems()
        {
            OrderItemsList.Clear();
        }
    }
}
