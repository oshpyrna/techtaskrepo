﻿using Caliburn.Micro;
using OrderManager.Client.Services;

namespace OrderManager.Client.ViewModels
{
    public class ShellViewModel : Screen
    {
        public OrderManagementViewModel OrderManagementVM { get; set; }

        public PlaceOrderViewModel PlaceOrderVM { get; set; }

        public ShellViewModel()
        {
            OrderManagementVM = IoC.Get<OrderManagementViewModel>();
            PlaceOrderVM = IoC.Get<PlaceOrderViewModel>();
            var test = IoC.Get<IDialogService>();
        }
    }
}
