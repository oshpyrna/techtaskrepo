﻿using Caliburn.Micro;

namespace OrderManager.Client.ViewModels
{
    public abstract class BaseViewModel : PropertyChangedBase
    {
        private bool _isAppNotBusy = true;
        public bool IsAppNotBusy
        {
            get
            {
                return _isAppNotBusy;
            }
            set
            {
                _isAppNotBusy =  value;
                NotifyOfPropertyChange(() => IsAppNotBusy);
            }
        }

        //TODO: Other stuff
    }
}
