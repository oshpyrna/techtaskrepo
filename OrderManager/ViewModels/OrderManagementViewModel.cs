﻿using Caliburn.Micro;
using OrderManager.Client.Services;
using OrderManager.Entities;
using System;
using System.Linq;

namespace OrderManager.Client.ViewModels
{
    public class OrderManagementViewModel : BaseViewModel
    {
        private readonly IOrderManagementService _orderService;
        private readonly IDialogService _dialogService;

        private OrderData _selectedOrder;

        public OrderData SelectedOrder
        {
            get
            {
                return _selectedOrder;
            }
            set
            {
                _selectedOrder = value;
                NotifyOfPropertyChange(() => SelectedOrder);
                NotifyOfPropertyChange(() => CanCompleteOrder);
            }
        }

        public bool CanCompleteOrder
        {
            get
            {
                return SelectedOrder != null
                    && SelectedOrder.OrderStatus != OrderStatusType.Completed;
            }
        }

        public BindableCollection<OrderData> OrdersList { get; set; }

        public OrderManagementViewModel(IOrderManagementService orderService, IDialogService dialogService)
        {
            _orderService = orderService;
            _dialogService = dialogService;

            OrdersList = new BindableCollection<OrderData>();
        }

        public async void LoadOrders()
        {
            IsAppNotBusy = false;

            OrdersList.Clear();

            try
            {
                var orders = await _orderService.GetOrders();
                OrdersList.AddRange(orders.OrderByDescending(o => o.OrderDate));
            }
            catch (Exception ex)
            {
                //TODO:
            }

            IsAppNotBusy = true;
        }

        public async void CompleteOrder()
        {
            IsAppNotBusy = false;

            try
            {
                var updatedOrder = await _orderService.CompleteOrder(SelectedOrder.Id);

                //TODO: Correct Update
                var orderIndex = OrdersList.IndexOf(SelectedOrder);
                OrdersList[orderIndex] = updatedOrder;

                SelectedOrder = updatedOrder;
            }
            catch (Exception ex)
            {
                //TODO:
            }

            IsAppNotBusy = true;
        }
    }
}
