﻿using Caliburn.Micro;
using Ninject;
using OrderManager.Client.OrderServiceReference;
using OrderManager.Client.Services;
using OrderManager.Client.ViewModels;
using OrderManager.Client.Views;
using System;
using System.Windows;
using System.Windows.Threading;

namespace OrderManager.Client
{
    public class AppBootstrapper : BootstrapperBase
    {
        private StandardKernel _container;

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            _container = new StandardKernel();

            _container.Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
            _container.Bind<IOrderService>().To<OrderServiceClient>().InSingletonScope();
            _container.Bind<IOrderManagementService>().To<OrderManagementService>();
            _container.Bind<IDialogService>().To<DialogService>().InSingletonScope();
            _container.Bind<IPlaceOrderService>().To<PlaceOrderService>().InSingletonScope();

            _container.Bind<OrderManagementViewModel>().ToSelf();
            _container.Bind<PlaceOrderViewModel>().ToSelf();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            MessageBox.Show(e.Exception.Message, "An Error has occurred.", MessageBoxButton.OK);
        }

        protected override object GetInstance(Type service, string key)
        {
            if (service == null)
                throw new ArgumentNullException("service");

            return _container.Get(service);
        }
    }
}
