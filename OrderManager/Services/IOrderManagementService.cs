﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Client.Services
{
    public interface IOrderManagementService
    {
        Task<List<OrderData>> GetOrders();

        Task<OrderData> CompleteOrder(long orderId);
    }
}
