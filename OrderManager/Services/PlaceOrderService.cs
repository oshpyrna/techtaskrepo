﻿using OrderManager.Client.OrderServiceReference;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Client.Services
{
    public class PlaceOrderService : IPlaceOrderService
    {
        private readonly IOrderService _serviceClient;

        public PlaceOrderService(IOrderService serviceClient)
        {
            _serviceClient = serviceClient;
        }

        public async Task<long> CreateOrder(OrderData order)
        {
            return await _serviceClient.CreateOrderAsync(order);
        }

        public async Task<List<CustomerData>> GetCustomers()
        {
            return await _serviceClient.GetCustomersAsync();
        }

        public async Task<List<ProductData>> GetProducts()
        {
            return await _serviceClient.GetProductsAsync();
        }
    }
}
