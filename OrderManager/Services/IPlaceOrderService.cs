﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Client.Services
{
    public interface IPlaceOrderService
    {
        Task<List<CustomerData>> GetCustomers();

        Task<List<ProductData>> GetProducts();

        Task<long> CreateOrder(OrderData order);
    }
}
