﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OrderManager.Client.Services
{
    public class DialogService : IDialogService
    {
        //TODO: Implement it in ShellView to use MahApps dialogs
        public void ShowNotification(string header, string message)
        {
            MessageBox.Show(message, header);
        }
    }
}
