﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Client.Services
{
    public interface IDialogService
    {
        void ShowNotification(string header, string message); 
    }
}
