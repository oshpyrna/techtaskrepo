﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderManager.Entities;
using OrderManager.Client.OrderServiceReference;

namespace OrderManager.Client.Services
{
    public class OrderManagementService : IOrderManagementService
    {
        private readonly IOrderService _serviceClient;

        public OrderManagementService(IOrderService orderService)
        {
            _serviceClient = orderService;
        }

        public async Task<List<OrderData>> GetOrders()
        {
            return await _serviceClient.GetOrdersAsync();
        }

        public async Task<OrderData> CompleteOrder(long orderID)
        {
            return await _serviceClient.CompleteOrderAsync(orderID);
        }
    }
}
