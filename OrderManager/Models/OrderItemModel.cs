﻿using Caliburn.Micro;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Client.Models
{
    public class OrderItemModel : PropertyChangedBase
    {
        public ProductData Product { get; private set; }

        public string ProductName
        {
            get
            {
                return Product.Name;
            }
        }

        private int _totalItems;
        public int TotalItems
        {
            get
            {
                return _totalItems;
            }
            set
            {
                _totalItems = value;
                NotifyOfPropertyChange(() => TotalItems);
                NotifyOfPropertyChange(() => TotalPrice);
            }
        }

        private decimal _price;
        public decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                NotifyOfPropertyChange(() => Price);
                NotifyOfPropertyChange(() => TotalPrice);
            }
        }

        public decimal TotalPrice
        {
            get
            {
                return Price * TotalItems;
            }
        }

        public OrderItemModel(ProductData productItem)
        {
            Product = productItem;
            TotalItems = 1;
            Price = 0.01M;
        }

        public OrderItemData GetOrderItem()
        {
            return new OrderItemData
            {
                ProductId = Product.Id,
                UnitPrice = Price,
                TotalPrice = Price * TotalItems,
                Quantity = TotalItems,
                OrderItemStatus = OrderItemStatusType.Created
            };
        }
    }
}
