﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace OrderManager.ServiceHost
{

    [ServiceContract]
    public interface IOrderService
    {
        [OperationContract]
        List<CustomerData> GetCustomers();

        [OperationContract]
        List<ProductData> GetProducts();

        [OperationContract]
        long CreateOrder(OrderData order);

        [OperationContract]
        List<OrderData> GetOrders();

        [OperationContract]
        OrderData CompleteOrder(long orderID);
    }
}
