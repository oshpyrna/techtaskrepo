﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using OrderManager.Entities;
using OrderManager.DAL.Repositories;
using OrderManager.OrderProcessor;

namespace OrderManager.ServiceHost
{
    public class OrderService : IOrderService
    {

        public List<CustomerData> GetCustomers()
        {
            return new CustomerProcessor().GetCustomers();
        }

        public List<ProductData> GetProducts()
        { 
            return new OrderProcessor.OrderProcessor().GetProducts();
        }

        public long CreateOrder(OrderData order)
        {
            return new OrderProcessor.OrderProcessor().CreateOrder(order);
        }

        public List<OrderData> GetOrders()
        {
            return new OrderProcessor.OrderProcessor().GetOrders();
        }

        public OrderData CompleteOrder(long orderID)
        {
            return new OrderProcessor.OrderProcessor().CompleteOrder(orderID);
        }
    }
}
