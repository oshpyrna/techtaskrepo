﻿using OrderManager.DAL;
using OrderManager.DAL.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Services;

namespace OrderManager.OrderService
{
    /// <summary>
    /// Summary description for OrderManagementService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class OrderManagementService : WebService
    {

        //[WebMethod]
        //public List<Customer> GetAllCustomers()
        //{
        //    //var customersRepository = new CustomerRepository(new DAL.DbContexts.OrdersManagementDbContext());
        //    //return customersRepository.GetAllCustomers();
        //    return new List<Customer>();
        //}

        [WebMethod]
        public List<Customer> GetCustomers()
        {
            var customerRepository = new CustomerRepository(new DAL.DbContexts.OrdersManagementDbContext());

            return customerRepository.GetAllCustomers();
        }
    }
}
