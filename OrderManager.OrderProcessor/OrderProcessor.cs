﻿using OrderManager.DAL.Repositories;
using OrderManager.DAL.Repositories.Interfaces;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderManager.OrderProcessor
{
    public class OrderProcessor
    {
        private readonly IProductRepository _productRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly NotificationProcessor _notificationService;

        //TODO: DI
        public OrderProcessor()
        {
            var repositoryFactory = new RepositoryFactory();
            _productRepository = repositoryFactory.GetProductsRepository();
            _orderRepository = repositoryFactory.GetOrderRepository();
            _notificationService = new NotificationProcessor();
        }

        //TODO: paging
        public List<OrderData> GetOrders()
        {
            return _orderRepository.GetAllOrders();
        }

        //TODO: paging
        public List<ProductData> GetProducts()
        {
            return _productRepository.GetAllProducts();
        }

        public OrderData GetOrderById(long orderId)
        {
            return _orderRepository.GetOrderById(orderId);
        }

        public long CreateOrder(OrderData order)
        {
            //TODO: validation of data and other stuff
            order.OrderDate = DateTime.UtcNow;

            return _orderRepository.CreateOrder(order);
        }

        public OrderData CompleteOrder(long orderId)
        {
            var order = _orderRepository.GetOrderById(orderId);

            foreach (var orderItem in order.OrderItems)
            {
                var isInStock = _productRepository.IsProductInStock(orderItem.ProductId);
                orderItem.OrderItemStatus = isInStock ? OrderItemStatusType.Processed
                    : OrderItemStatusType.NotInStock;
            }

            _orderRepository.UpdateOrderItemsStatus(order.OrderItems);

            var isOrderCompleted = order.OrderItems.All(oi => oi.OrderItemStatus == OrderItemStatusType.Processed);
            order.OrderStatus = isOrderCompleted ? OrderStatusType.Completed : OrderStatusType.PartiallyCompleted;

            var updatedOrder = _orderRepository.UpdateOrder(order);
            _notificationService.NotifyOrderStatus(order);

            return updatedOrder;
        }
    }
}
