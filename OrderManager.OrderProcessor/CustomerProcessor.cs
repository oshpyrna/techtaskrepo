﻿using OrderManager.DAL.Repositories;
using OrderManager.DAL.Repositories.Interfaces;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.OrderProcessor
{
    public class CustomerProcessor
    {
        private readonly ICustomerRepository _customerRepository;
        
        //TODO: DI
        public CustomerProcessor()
        {
            _customerRepository = new RepositoryFactory().GetCustomerRepository();
        }

        //TODO: Paging
        public List<CustomerData> GetCustomers()
        {
            return _customerRepository.GetAllCustomers();
        }
    }
}
