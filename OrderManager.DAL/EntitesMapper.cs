﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.DAL
{
    public class EntitiesMapper
    {
        static EntitiesMapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Customer, CustomerData>();
                cfg.CreateMap<CustomerData, Customer>();
                cfg.CreateMap<Product, ProductData>();
                cfg.CreateMap<ProductData, Product>();
                cfg.CreateMap<Order, OrderData>();
                cfg.CreateMap<OrderData, Order>();
                cfg.CreateMap<OrderItem, OrderItemData>();
                cfg.CreateMap<OrderItemData, OrderItem>();
            });
        }

        public static TDestination Map<TDestination>(object source)
        {
            return AutoMapper.Mapper.Map<TDestination>(source);
        }
    }
}
