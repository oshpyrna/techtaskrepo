﻿using OrderManager.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderManager.Entities;
using OrderManager.DAL.DbContexts;

namespace OrderManager.DAL.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductOrdersDbContext _dbContext;

        public ProductRepository(ProductOrdersDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<ProductData> GetAllProducts()
        {
            var products = _dbContext.Products.ToList();

            return products.Select(p => EntitiesMapper.Map<ProductData>(p)).ToList();
        }

        public ProductData GetProductById(int id)
        {
            var product = _dbContext.Products.Find(id);

            if(product != null)
            {
                return EntitiesMapper.Map<ProductData>(product);
            }
            else
            {
                return null;
            }
        }

        public bool IsProductInStock(int productId)
        {
            return _dbContext.Products.Find(productId).IsInStock;
        }
    }
}
