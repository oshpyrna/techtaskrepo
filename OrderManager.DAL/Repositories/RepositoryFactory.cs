﻿using OrderManager.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.DAL.Repositories
{
    public class RepositoryFactory
    {
        private readonly DbContexts.ProductOrdersDbContext _dbContext;

        //TODO: DI Contatiner
        public RepositoryFactory()
        {
            _dbContext = new DbContexts.ProductOrdersDbContext();
        }

        public ICustomerRepository GetCustomerRepository()
        {
            return new CustomerRepository(_dbContext);
        }

        public IOrderRepository GetOrderRepository()
        {
            return new OrderRepository(_dbContext);
        }

        public IProductRepository GetProductsRepository()
        {
            return new ProductRepository(_dbContext);
        }
    }
}
