﻿using OrderManager.DAL.DbContexts;
using OrderManager.DAL.Repositories.Interfaces;
using OrderManager.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System;
using System.Data.Entity.Migrations;

namespace OrderManager.DAL.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ProductOrdersDbContext _dbContext;

        public OrderRepository(ProductOrdersDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public long CreateOrder(OrderData orderData)
        {
            try
            {
                var order = EntitiesMapper.Map<Order>(orderData);
                _dbContext.Orders.Add(order);
                _dbContext.SaveChanges();

                return order.Id;
            }
            catch (Exception ex)
            {

                throw;
            }


            
        }

        public List<OrderData> GetAllOrders()
        {
            var orders = _dbContext.Orders.ToList();

            return orders.Select(o => EntitiesMapper.Map<OrderData>(o)).ToList();
        }

        public OrderData GetOrderById(long orderId)
        {
            var order = _dbContext.Orders.Find(orderId);

            if(order != null)
            {
                return EntitiesMapper.Map<OrderData>(order);
            }
            else
            {
                return null;
            }
        }

        public OrderData UpdateOrder(OrderData orderData)
        {
            var newOrder = EntitiesMapper.Map<Order>(orderData);
            var existingOrder = _dbContext.Orders.Find(orderData.Id);

            _dbContext.Entry(existingOrder).CurrentValues.SetValues(newOrder);
            _dbContext.SaveChanges();

            return EntitiesMapper.Map<OrderData>(existingOrder);
        }

        public void UpdateOrderItemsStatus(IList<OrderItemData> orderItemsData)
        {
            foreach (var orderItemData in orderItemsData)
            {
                var orderitem = _dbContext.OrderItems.Find(orderItemData.Id);
                orderitem.OrderItemStatusId = orderItemData.OrderItemStatusId;
            }

            _dbContext.SaveChanges();
        }
    }
}
