﻿using AutoMapper;
using OrderManager.DAL.DbContexts;
using OrderManager.DAL.Repositories.Interfaces;
using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderManager.DAL.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ProductOrdersDbContext _dbContext;

        public CustomerRepository(ProductOrdersDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<CustomerData> GetAllCustomers()
        {
            var customers = _dbContext.Customers.ToList();

            return customers.Select(c => EntitiesMapper.Map<CustomerData>(c)).ToList();
        }

        public CustomerData GetCustomerById(Guid customerId)
        {
            var customer = _dbContext.Customers
                .Where(c => c.Id == customerId)
                .FirstOrDefault();

            if(customer != null)
            {
                return EntitiesMapper.Map<CustomerData>(customer);
            }
            else
            {
                return null;
            }
            
        }
    }
}
