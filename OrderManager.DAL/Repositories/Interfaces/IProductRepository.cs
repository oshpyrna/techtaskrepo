﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.DAL.Repositories.Interfaces
{
    public interface IProductRepository
    {
        List<ProductData> GetAllProducts();

        ProductData GetProductById(int id);

        bool IsProductInStock(int productId);
    }
}
