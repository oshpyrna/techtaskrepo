﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.DAL.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        long CreateOrder(OrderData order);

        OrderData GetOrderById(long orderId);

        List<OrderData> GetAllOrders();

        OrderData UpdateOrder(OrderData order);

        void UpdateOrderItemsStatus(IList<OrderItemData> orderItemsData);
    }
}
