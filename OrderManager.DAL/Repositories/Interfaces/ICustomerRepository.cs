﻿using OrderManager.Entities;
using System;
using System.Collections.Generic;

namespace OrderManager.DAL.Repositories.Interfaces
{
    public interface ICustomerRepository
    {
        List<CustomerData> GetAllCustomers();

        CustomerData GetCustomerById(Guid customerId);
    }
}
